import com.google.gson.Gson;
import data.DataManager;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import models.Board;
import models.Card;
import org.apache.http.HttpStatus;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import utils.RequestFactory;
import base.BaseTest;
import io.restassured.RestAssured;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Random;

import static utils.AutomationProcess.*;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Trello extends BaseTest {


    @Test
    public void stage1_CreateBoard() {
        WebTest_Automation_Task_Test(1, "Create Board");
        WebTest_Automation_Task_Create_Page("Create Board");

        Response response = RestAssured
                .given()
                .contentType(ContentType.TEXT)
                .queryParam("key", DataManager.API_KEY.getData())
                .queryParam("token", DataManager.TOKEN.getData())
                .queryParam("name", "Test2")
                .when()
                .post(RestAssured.baseURI + "/boards/")
                .andReturn();

        response.then()
                .statusCode(HttpStatus.SC_OK);

        Board empty = new Gson().fromJson(response.asString(), Board.class);
        RequestFactory.addBoard(empty);
        WebTest_Automation_Task_Finish_Page("Create Board");
    }



    @Test
    public void stage2_CreateCard() {
        WebTest_Automation_Task_Test(2, "Create Card");
        WebTest_Automation_Task_Create_Page("Create Card");
        Board temp = RequestFactory.getBoard(0);

        Response responseList = RestAssured
                .given()
                .contentType(ContentType.TEXT)
                .queryParam("key", DataManager.API_KEY.getData())
                .queryParam("token", DataManager.TOKEN.getData())
                .queryParam("name", "DenemeList")
                .when()
                .post(RestAssured.baseURI + "/boards/" + temp.getId() + "/lists")
                .andReturn();

        responseList.then()
                .statusCode(HttpStatus.SC_OK);

        String listID = responseList.getBody().jsonPath().get("id");

        for (int i = 0; i < DataManager.CARD_NUMBER.getDataInt(); i++) {
            Response response = RestAssured
                    .given()
                    .contentType(ContentType.TEXT)
                    .queryParam("key", DataManager.API_KEY.getData())
                    .queryParam("token", DataManager.TOKEN.getData())
                    .queryParam("idList", listID)
                    .queryParam("name", "Card" + (i + 1))
                    .when()
                    .post(RestAssured.baseURI + "/cards")
                    .andReturn();

            response.then()
                    .statusCode(HttpStatus.SC_OK);


            Card card = response.as(Card.class);
            RequestFactory.addCard(card);
        }
        WebTest_Automation_Task_Finish_Page("Create Card");
    }



    @Test
    public void stage3_UpdateToCard() {
        WebTest_Automation_Task_Test(3, "Update To Card");
        WebTest_Automation_Task_Create_Page("Update To Card");

        int rand = new Random().nextInt(2);
        Card card = RequestFactory.getCard(rand);

        Response response = RestAssured
                .given()
                .contentType(ContentType.TEXT)
                .queryParam("key", DataManager.API_KEY.getData())
                .queryParam("token", DataManager.TOKEN.getData())
                .queryParam("name", "Change Card")
                .when()
                .put(RestAssured.baseURI + "/cards/" + card.getId())
                .andReturn();

        response.then()
                .statusCode(HttpStatus.SC_OK);
        WebTest_Automation_Task_Finish_Page("Update To Card");
    }

    @Test
    public void stage4_RemoveCard() {
        WebTest_Automation_Task_Test(4, "Remove Card");
        WebTest_Automation_Task_Create_Page("Remove Card");
        ArrayList<Card> boardList = RequestFactory.getCardList();

        for (Card card : boardList) {
            Response response = RestAssured
                    .given()
                    .contentType(ContentType.TEXT)
                    .queryParam("key", DataManager.API_KEY.getData())
                    .queryParam("token", DataManager.TOKEN.getData())
                    .when()
                    .delete(RestAssured.baseURI + "/cards/" + card.getId())
                    .andReturn();

            response.then()
                    .statusCode(HttpStatus.SC_OK);
        }
        WebTest_Automation_Task_Finish_Page("Remove Card");
    }

    @Test
    public void stage5_RemoveBoard() {
        WebTest_Automation_Task_Test(5, "Remove Board");
        WebTest_Automation_Task_Create_Page("Remove Board");
        ArrayList<Board> boardList = RequestFactory.getBoardList();

        for (Board board : boardList) {
            Response response = RestAssured
                    .given()
                    .contentType(ContentType.TEXT)
                    .queryParam("key", DataManager.API_KEY.getData())
                    .queryParam("token", DataManager.TOKEN.getData())
                    .when()
                    .delete(RestAssured.baseURI + "/boards/" + board.getId())
                    .andReturn();

            response.then()
                    .statusCode(HttpStatus.SC_OK);
        }
        WebTest_Automation_Task_Finish_Page("Remove Board");
    }
}
