package models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Card {

    private String id;
    private List<Object> checkItemStates = null;
    private Boolean closed;
    private Boolean dueComplete;
    private String dateLastActivity;
    private String desc;
    private Object descData;
    private Object due;
    private Object email;
    private String idBoard;
    private List<Object> idChecklists = null;
    private String idList;
    private List<String> idMembers = null;
    private List<Object> idMembersVoted = null;
    private Integer idShort;
    private String idAttachmentCover;
    private Boolean manualCoverAttachment;
    private List<String> idLabels = null;
    private String name;
    private Integer pos;
    private String shortLink;
    private String shortUrl;
    private Boolean subscribed;
    private String url;
    private String address;
    private String locationName;

    private Map<String, Object> additionalProperties = new HashMap<>();

    public Card() {
    }

    public Card(String id, List<Object> checkItemStates, Boolean closed, Boolean dueComplete, String dateLastActivity, String desc, Object descData, Object due, Object email, String idBoard, List<Object> idChecklists, String idList, List<String> idMembers, List<Object> idMembersVoted, Integer idShort, String idAttachmentCover, Boolean manualCoverAttachment, List<String> idLabels, String name, Integer pos, String shortLink, String shortUrl, Boolean subscribed, String url, String address, String locationName, Map<String, Object> additionalProperties) {
        this.id = id;
        this.checkItemStates = checkItemStates;
        this.closed = closed;
        this.dueComplete = dueComplete;
        this.dateLastActivity = dateLastActivity;
        this.desc = desc;
        this.descData = descData;
        this.due = due;
        this.email = email;
        this.idBoard = idBoard;
        this.idChecklists = idChecklists;
        this.idList = idList;
        this.idMembers = idMembers;
        this.idMembersVoted = idMembersVoted;
        this.idShort = idShort;
        this.idAttachmentCover = idAttachmentCover;
        this.manualCoverAttachment = manualCoverAttachment;
        this.idLabels = idLabels;
        this.name = name;
        this.pos = pos;
        this.shortLink = shortLink;
        this.shortUrl = shortUrl;
        this.subscribed = subscribed;
        this.url = url;
        this.address = address;
        this.locationName = locationName;
        this.additionalProperties = additionalProperties;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Object> getCheckItemStates() {
        return checkItemStates;
    }

    public void setCheckItemStates(List<Object> checkItemStates) {
        this.checkItemStates = checkItemStates;
    }

    public Boolean getClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    public Boolean getDueComplete() {
        return dueComplete;
    }

    public void setDueComplete(Boolean dueComplete) {
        this.dueComplete = dueComplete;
    }

    public String getDateLastActivity() {
        return dateLastActivity;
    }

    public void setDateLastActivity(String dateLastActivity) {
        this.dateLastActivity = dateLastActivity;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Object getDescData() {
        return descData;
    }

    public void setDescData(Object descData) {
        this.descData = descData;
    }

    public Object getDue() {
        return due;
    }

    public void setDue(Object due) {
        this.due = due;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public String getIdBoard() {
        return idBoard;
    }

    public void setIdBoard(String idBoard) {
        this.idBoard = idBoard;
    }

    public List<Object> getIdChecklists() {
        return idChecklists;
    }

    public void setIdChecklists(List<Object> idChecklists) {
        this.idChecklists = idChecklists;
    }

    public String getIdList() {
        return idList;
    }

    public void setIdList(String idList) {
        this.idList = idList;
    }

    public List<String> getIdMembers() {
        return idMembers;
    }

    public void setIdMembers(List<String> idMembers) {
        this.idMembers = idMembers;
    }

    public List<Object> getIdMembersVoted() {
        return idMembersVoted;
    }

    public void setIdMembersVoted(List<Object> idMembersVoted) {
        this.idMembersVoted = idMembersVoted;
    }

    public Integer getIdShort() {
        return idShort;
    }

    public void setIdShort(Integer idShort) {
        this.idShort = idShort;
    }

    public String getIdAttachmentCover() {
        return idAttachmentCover;
    }

    public void setIdAttachmentCover(String idAttachmentCover) {
        this.idAttachmentCover = idAttachmentCover;
    }

    public Boolean getManualCoverAttachment() {
        return manualCoverAttachment;
    }

    public void setManualCoverAttachment(Boolean manualCoverAttachment) {
        this.manualCoverAttachment = manualCoverAttachment;
    }

    public List<String> getIdLabels() {
        return idLabels;
    }

    public void setIdLabels(List<String> idLabels) {
        this.idLabels = idLabels;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPos() {
        return pos;
    }

    public void setPos(Integer pos) {
        this.pos = pos;
    }

    public String getShortLink() {
        return shortLink;
    }

    public void setShortLink(String shortLink) {
        this.shortLink = shortLink;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public Boolean getSubscribed() {
        return subscribed;
    }

    public void setSubscribed(Boolean subscribed) {
        this.subscribed = subscribed;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "CardTrello{" +
                "id='" + id + '\'' +
                ", checkItemStates=" + checkItemStates +
                ", closed=" + closed +
                ", dueComplete=" + dueComplete +
                ", dateLastActivity='" + dateLastActivity + '\'' +
                ", desc='" + desc + '\'' +
                ", descData=" + descData +
                ", due=" + due +
                ", email=" + email +
                ", idBoard='" + idBoard + '\'' +
                ", idChecklists=" + idChecklists +
                ", idList='" + idList + '\'' +
                ", idMembers=" + idMembers +
                ", idMembersVoted=" + idMembersVoted +
                ", idShort=" + idShort +
                ", idAttachmentCover='" + idAttachmentCover + '\'' +
                ", manualCoverAttachment=" + manualCoverAttachment +
                ", idLabels=" + idLabels +
                ", name='" + name + '\'' +
                ", pos=" + pos +
                ", shortLink='" + shortLink + '\'' +
                ", shortUrl='" + shortUrl + '\'' +
                ", subscribed=" + subscribed +
                ", url='" + url + '\'' +
                ", address='" + address + '\'' +
                ", locationName='" + locationName + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}